<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->integer('shop_id')->unsigned();
            $table->integer('order_id')->unsigned();
            $table->integer('status')->unsigned();
            $table->float('order_price');
            $table->char('currency', 5);
            $table->dateTime('timestamp');

            $table->primary(['shop_id', 'order_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('orders');
    }
}
