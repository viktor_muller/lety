<?php
namespace App\Services;
use App\Services\ApiRequests\AbstractApiRequest;

class RequestDataDecoder
{
    
    const JSON_CONTENT_TYPE = 'application/json';
    const XML_CONTENT_TYPE = 'application/xml';
    const CSV_CONTENT_TYPE = 'text/csv';

    /**
     * @var \App\Services\ApiRequests\AbstractApiRequest
     */
    private $req;
    
    public function __construct(AbstractApiRequest $req)
    {
        $this->req = $req;
    }
    
    public function decode()
    {
        $request_content_type = $this->req->getContentType();
        if(self::JSON_CONTENT_TYPE == $request_content_type) {
            return $this->decodeJson();
        } elseif(self::XML_CONTENT_TYPE == $request_content_type) {
            return $this->decodeXML();
        } elseif(self::CSV_CONTENT_TYPE == $request_content_type) {
            return $this->decodeCSV();
        }
    }
    
    private function decodeJson()
    {
        
    }
    
    private function decodeXML()
    {
        $xml = $this->req->getData();

        $xml_obj = simplexml_load_string($xml, "SimpleXMLElement", LIBXML_NOCDATA);
        $json = json_encode($xml_obj);
        $array = json_decode($json,TRUE);

        return $array;
    }

    private function decodeCSV()
    {
        $csv = $this->req->getData();
    }
}