<?php
namespace App\Services\DataAdapters;

abstract class AbstractDataAdapter
{
    abstract public function formatData($decoded_data);
}