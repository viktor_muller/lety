<?php
namespace App\Services\DataAdapters;

use App\Services\ApiRequests\ApiEbayRequest;

class EbayDataAdapter extends AbstractDataAdapter
{
    public function formatData()
    {
        $api_request = new ApiEbayRequest();
        $api_request->getData();
    }
}