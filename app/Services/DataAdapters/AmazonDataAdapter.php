<?php
namespace App\Services\DataAdapters;

class AmazonDataAdapter extends AbstractDataAdapter
{
    public function formatData( $decoded_data )
    {
        $formated_data = array();

        $data = $decoded_data['stat'];
        foreach ($data as $item) {

            $formated_item = array();
            $formated_item['shop_id']       = $item['advcampaign_id'];
            $formated_item['order_id']      = $item['order_id'];
            $formated_item['status']        = $item['status'];
            $formated_item['order_price']   = $item['cart'];
            $formated_item['currency']      = $item['currency'];
            $formated_item['timestamp']     = $item['action_date'];

            array_push($formated_data, $formated_item);
        }
        return $formated_data;
    }
}