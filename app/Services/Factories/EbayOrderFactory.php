<?php
namespace App\Services\Factories;

use App\Services\ApiRequests\ApiEbayRequest;
use App\Services\RequestDataDecoder;

class EbayOrderFactory extends AbstractOrdersFactory
{
    public function __construct(){
        $this->getDecodedReqData();
    }

    public function getDecodedReqData()
    {
        $api_ebay_request = new ApiEbayRequest();
        $request_data_decoder = new RequestDataDecoder( $api_ebay_request );

        return $request_data_decoder->decode();
    }

    public function getOrdersList()
    {
        // TODO: Implement getOrdersList() method.
    }
}