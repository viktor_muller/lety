<?php
namespace App\Services\Factories;

abstract class AbstractOrdersFactory
{
    
    public static function createOrder($storeName)
    {
        if ($storeName == 'Amazon') {
            return new AmazonOrderFactory();
        } elseif ($storeName == 'Ebay') {
            return new EbayOrderFactory();
        }
    }
    
    abstract public function getDecodedReqData();
    
    abstract public function getOrdersList();
}