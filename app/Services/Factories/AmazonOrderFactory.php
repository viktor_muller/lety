<?php
namespace App\Services\Factories;

use App\Services\ApiRequests\ApiAmazonRequest;
use App\Services\DataAdapters\AmazonDataAdapter;
use App\Services\RequestDataDecoder;

class AmazonOrderFactory extends AbstractOrdersFactory
{
    public function __construct(){
        return $this->getOrdersList();
    }

    public function getDecodedReqData()
    {
        $api_amazon_request = new ApiAmazonRequest();
        $request_data_decoder = new RequestDataDecoder( $api_amazon_request );

        return $request_data_decoder->decode();
    }

    public function getOrdersList()
    {
        $decoded_data = $this->getDecodedReqData();
        $amazon_data_adapter = new AmazonDataAdapter();
        return $amazon_data_adapter->formatData( $decoded_data );
    }
}