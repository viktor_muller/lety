<?php
namespace App\Services;

use DB;
use App\Services\Factories\AbstractOrdersFactory;

class OrderService
{
    public function __construct()
    {
        $shops_names = $this->getShopsNames();
        foreach ($shops_names as $shop_name) {
            $orders = AbstractOrdersFactory::createOrder($shop_name);
            $orders_list = $orders->getOrdersList();
            $this->saveOrders( $orders_list );
        }
    }

    public function saveOrders($orders)
    {
        if(is_array($orders) && !empty($orders)) {
            // TODO: SQL on dublicate key update
           DB::table('orders')->insert($orders);
        }
    }

    public function getShopsNames()
    {
        return ['Amazon', 'Ebay'];
    }

}