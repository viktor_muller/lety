<?php
namespace App\Services\ApiRequests;

class ApiEbayRequest extends AbstractApiRequest
{

    public function getData()
    {
        if($this->Auth()) {
            $content = file_get_contents('csv.csv');
            return $content;
        }
    }

    public function getContentType()
    {
        return 'text/csv';
    }

    protected function Auth()
    {
        return true;
    }
}