<?php
namespace App\Services\ApiRequests;

abstract class AbstractApiRequest
{
    // Авторизация, baseURL, получение данных
    abstract public function getData();
    abstract public function getContentType();
    abstract protected function Auth();
    
}