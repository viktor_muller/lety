<?php
namespace App\Services\ApiRequests;

class ApiAmazonRequest extends AbstractApiRequest
{
    public function getData()
    {   
        if($this->Auth()) {
            $content = file_get_contents('xml.xml');
            return $content;
        }
    }
    
    public function getContentType()
    {
        return 'application/xml';
    }

    protected function Auth()
    {
        return true;
    }
}